/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.robotproject;

/**
 *
 * @author nasan
 */
public class TestRobot {
    public static void main(String[] args) {
        Robot robot = new Robot(0,0,3,16,100);
        System.out.println(robot);
        robot.walk('S');
        System.out.println(robot);
        
        robot.walk('N', 2);
        System.out.println(robot);
        
        robot.walk('E', 5);
        System.out.println(robot);
        
        robot.walk();
        System.out.println(robot);
        
        robot.walk('S');
        System.out.println(robot);
        
        robot.walk(15);
        System.out.println(robot);
        
        robot.walk('W', 3);
        System.out.println(robot);
    }
}
